# Stub SaaS Framework

## Goal
There's a bunch of various "plumbing" that needs to be setup for every single tenant (just a website for one company) and multi-tenant (multiple companies and users share the same code, ie SaaS) site/project I build. 

With that in mind I've tried to keep a generic and basic set of functionality in this repo with most of the generic plumbing that all sites need.
- Accounts
- Users (customers and yourself or employees)
- Ability to manage those accounts and users
- Public pages (not logged in)
- User pages (logged in as a customer)
- Admin pages  (logged in as an employee / administrator)


## Features

[Symfony](https://symfony.com/) project that includes the following:

**Firewall / Authentication** 
- Login form and authenticator using RDBMS 
- API Key and token authenticator using RDMS
- Firewall rules for routes `/admin`, `/account`, and `/api`
- User Roles `ROLE_USER`, `ROLE_ADMIN` and `ROLE_API`
- Lost password reset functionality with TTL token
- Password complexity requirement settings
- Remember Me login functionality
- Option for user self-signup / registration
- Token Authorization for API endpoints
- IP based authentication for Flowroute callback endpoint

**Entities**
- Entities with uuid for the unique id instead of auto increment 
    - `Account`
    - `User`
    - `UserNote` (leave notes about customer interactions, followups, etc)
    - `AccountNote` 
    - `AccountFeature` (feature toggle enablement)
    - `ApiKey` (api enablement)
    - `FlowrouteMessage` (SMS & MMS text messaging with Flowroute provider)
    - `FlowrouteMessageMedia` (file attachments to MMS)
- UserFixture for two accounts and three users:
    - `admin@example.com`:`adminpassword`
    - `user@example.com`: `userpassword`
    - `user2@example.com`: `user2password`
- Login Listener for auto updating user last login


**Controllers / Templates**
- Stub templates and controllers for four primary sections: `/` (public site), `/admin`, `/account`, and `/api`
- Custom 400, 403, 404, 500, and generic error pages.
- Includes bootstrap4, jquery, fontawesome already
- Responsive Email templates (lost password, welcome email)
- Responsive layout (works on mobile devices)
- Admin pages with functionality for 
    - List accounts
    - Create Account
    - List users 
    - List users by account 
    - Create users
    - Edit users
    - Add notes to users
    - Add notes to accounts
    - Global search for admins
    - Configuration testing - send SMS/MMS and Emails to verify your configuration
- Account pages with functionality for
    - Feature Toggling (Currently only API Keys)
    - List API Keys
    - Create API Keys
    - Edit API Keys

**Other**
- Send and receive SMS (text messages)
- Send and receive MMS (text messages with media/pictures)


# Requirements
- PHP 7+
- [Composer](https://getcomposer.org/)
- A relational database ([MySQL](https://www.mysql.com/), [PostgreSQL](https://www.postgresql.org/), [Maria DB](https://mariadb.org/), etc.)
- SMTP server, or Gmail, AWS SES, SendGrid; some service to send emails

# Installing
- Download, checkout, or fork the repo
```
$ cd stub-saas/
$ cp env-sample .env
```

Make sure to edit default settings in `.env`, setup your database, sendgrid key, and other specific attributes at the bottom of the file.

```
$ composer install
$ bin/console doctrine:database:create --force
$ bin/console doctrine:schema:create --force
# Load in the sample data
$ bin/console doctrine:fixtures:load  
// Start a local webserver
$ `symfony server:start`  or `php -S localhost:8001`
```

# Next Steps For You
**Use this as a building block for whatever you're building.**
- Setup your SMTP / Emailing provider - see the [Mail doc](/Mail.md)
- Optionally setup SMS/MMS text messaging via Flowroute provider - see the [Flowroute doc](/Flowroute.md)
- Add addtional entities, business logic and functionality.
- Add your own theme and layouts.
- Profit! 