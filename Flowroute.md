# Flowroute

## SMS/MMS Text Messaging Documentation
- Symfony has documentation that supports sending SMS only (no MMS) via
    - Twilio, Nexmo, OvhCloud, Sinch, and FreeMobile
    - The Notifier supports SMS, Chat, Email and many other features.
    - See details https://symfony.com/doc/current/notifier/texters.html

    `` The Symfony Notifier does not support Flowroute``


## Flowroute

- https://www.flowroute.com/
- After sign-up, and login - on the left navigation 
    - Click DIDs, then Purchase
        - Search for and purchase a 10 digit or toll free vanity number.
    - Click DIDs, select Action -> Enable Messaging
    - Click Preferences and then API Control
        - Enable SMS and MMS callbacks (specify the url to your /flowroute/message endpoint for both)
        - DLR keep disabled as it's not implimented yet.
    - Setup a new API Key and put in your .env 


### Authentication
See also the Firewall and Authentication section of [README doc](/README.md)
- Whitelisted IPs `52.88.246.140`, `52.10.220.50`
- Older inbound documentation https://blog.flowroute.com/2016/09/22/receive-an-inbound-message/


## Using another provider
The code for FlowrouteMessageService is very specific to Flowroute. You could create a new service that impliments the MessageServiceInterface, install Symfony Notifier, or another option of your choosing. There is nothing to uninstall if you go this route.