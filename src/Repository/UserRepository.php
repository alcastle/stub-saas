<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Account;
use App\Entity\UserNote;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param string $email
     * @see LoginFormAuthenticator
     * 
     * Find a user by email that isn't deleted
     * and the user is marked as active 
     * and the user is verified (this happens when they verify their account)
     * and their account is not deleted
     * and their account is active
     */
    public function loginUser(string $email)
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->andWhere('u.isDeleted = 0')
            ->andWhere('u.isActive = 1')
            ->andWhere('u.isVerified = 1')
            ->leftJoin('u.account', 'a')
            ->andWhere('a.isActive = 1')
            ->andWhere('a.isDeleted = 0')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

   

    /**
     * Find user that isnt deleted by email address
     */
    public function findEmail(string $email): ?User 
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :val')
            ->andWhere('u.isDeleted = 0')
            ->setParameter('val', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find the password_token for a non-deleted account 
     * Make sure the token hasn't expired
     */
    public function findToken(string $token, $ttl = 2): ?User 
    {
        $now     = new \DateTime();
        $hours   = new \DateTime();
        $hours->add(new \DateInterval('PT'.$ttl.'H'));

        return $this->createQueryBuilder('u')
            ->andWhere('u.passwordToken = :val')
            ->andWhere('u.isDeleted = 0')
            ->andWhere('u.tokenExpiresAt > :now AND u.tokenExpiresAt <= :hours')
            ->setParameter('val', $token)
            ->setParameter('now', $now)
            ->setParameter('hours', $hours)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find user that isn't deleted by uuid / id
     */
    public function findById($uuid)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.id = :val')
            ->andWhere('u.isDeleted = 0')
            ->setParameter('val', $uuid)
            ->getQuery()
            ->getOneOrNullResult();
    }
  
    public function countAll()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActive()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.isActive = 1')
            ->getQuery()
            ->getSingleScalarResult();  
    }

    public function countAllByAccount(Account $account)
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.account = :account')
            ->setParameter('account', $account)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActiveByAccount(Account $account)
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.isActive = 1')
            ->andWhere('u.account = :account')
            ->setParameter('account', $account->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllByAccount(Account $account)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.account = :account')
            ->setParameter('account', $account->getId())
            ->getQuery()
            ->getResult();  
    }

    /**
     * @param string $search
     * @return array 
     * 
     * Finds a list of distinct user non-admin user ids 
     * that match our keyword search.
     */
    public function adminSearch($search)
    {
        $sql = '
        SELECT 
            DISTINCT(u.id)
        FROM user AS u 
        LEFT JOIN user_note AS un on un.user_id = u.id 
        LEFT JOIN account AS a on u.account_id = a.id 
        LEFT JOIN account_note AS an on a.id = an.account_id 
        WHERE u.is_deleted = 0
        AND a.is_deleted = 0
        AND u.roles = \'["ROLE_USER"]\'
        AND (
            u.first_name LIKE :search OR 
            u.last_name LIKE :search OR
            u.email LIKE :search OR
            a.name LIKE :search OR
            a.website LIKE :search OR
            a.allowed_domains LIKE :search OR
            un.note LIKE :search OR
            an.note LIKE :search 
            )
        LIMIT 20';

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute(['search' => '%'.$search.'%']);
        
        return $stmt->fetchAll();

        /*
        return $this->createQueryBuilder('u')
            ->andWhere('
                u.isDeleted = 0 AND 
                u.email LIKE :search OR 
                u.firstName LIKE :search OR     
                u.lastName LIKE :search
                ')
            ->leftJoin('u.account', 'a')
            ->andWhere('a.isDeleted = 0')
            ->orWhere('
                a.name LIKE :search OR
                a.website LIKE :search OR
                a.allowedDomains LIKE :search
                ')
            #->innerJoin(UserNote::class, 'un', Join::WITH, 'un.user = u')
            #->orWhere('un.note LIKE :search')
            ->orderBy('u.createdAt', 'DESC')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult();
            */
    }

    public function generalSearch(string $search)
    {
        // TODO build out what to search for from a user (non-admin) perspective
        $sql    = '';
        $conn   = $this->getEntityManager()->getConnection();
        $stmt   = $conn->prepare($sql);
        $stmt->execute(['search' => '%'.$search.'%']);
        
        return $stmt->fetchAll();
    }

    /**
     * Find the API User with ROLE_API by the account
     * 
     */
    public function getApiUser(Account $account)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.account = :account')
            ->andWhere('u.roles LIKE :role')
            
            ->leftJoin('u.account', 'a')
            ->andWhere('a.isActive = 1')
            ->andWhere('a.isDeleted = 0')
            
            ->leftJoin('a.accountFeatures', 'f')
            ->andWhere('f.name = :enabled')
            ->andWhere('f.value = 1')

            ->setParameter('role', '"ROLE_API"')
            ->setParameter('enabled', 'apiEnabled')
            ->setParameter('account', $account)
            ->getQuery()
            ->getOneOrNullResult();    
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
