<?php

namespace App\Repository;

use App\Entity\FlowrouteMessageMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FlowrouteMessageMedia|null find($id, $lockMode = null, $lockVersion = null)
 * @method FlowrouteMessageMedia|null findOneBy(array $criteria, array $orderBy = null)
 * @method FlowrouteMessageMedia[]    findAll()
 * @method FlowrouteMessageMedia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlowrouteMessageMediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FlowrouteMessageMedia::class);
    }

}
