<?php

namespace App\Repository;

use App\Entity\AccountFeature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use App\Entity\Account;


/**
 * @method AccountFeature|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountFeature|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountFeature[]    findAll()
 * @method AccountFeature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountFeatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountFeature::class);
    }

    public function apiEnabled(Account $account)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.account = :account')
            ->andWhere('f.name = :enabled')
            ->andWhere('f.value = 1')
            ->setParameter('account', $account)
            ->setParameter('enabled', 'apiEnabled')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Retrieve a list of unique features 
     */
    public function listFeatures()
    {
        return $this->createQueryBuilder('k')
            ->select('DISTINCT k.name')
            ->getQuery()
            ->getResult();
    }

    public function featureByAccount(Account $account)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.account = :account')
            ->setParameter('account', $account)
            ->getQuery()
            ->getResult();
    }
}
