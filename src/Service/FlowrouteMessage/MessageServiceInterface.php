<?php

namespace App\Service\FlowrouteMessage;


interface MessageServiceInterface
{

    public function setTo(string $to);

    public function setFrom(string $from);

    public function setMessage(string $message);

    public function setImages(array $images);

    public function sendSMS();

    public function sendMMS();

    public function send(array $attributes);

}