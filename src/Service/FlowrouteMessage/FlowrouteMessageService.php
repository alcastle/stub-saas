<?php

namespace App\Service\FlowrouteMessage;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\FlowrouteMessage;
use App\Entity\FlowrouteMessageMedia;
use App\Service\FlowrouteMessage\MessageServiceInterface;

class FlowrouteMessageService implements MessageServiceInterface 
{
    const SMS           = 'SMS';
    const MMS           = 'MMS';
    const ERROR_NUMBER  = '5551234567';
    const INBOUND       = 'inbound';
    const OUTBOUND      = 'outbound';

    private $em;
    private $router;
    private $params;

    private $accessKey;
    private $secretKey;
    
    private $client;
    private $headers;
    private $baseUrl;
    private $toNumber;
    private $fromNumber;
    private $message;
    private $images;
    private $dlrCallback;


    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $router, ParameterBagInterface $params)
    {
        $this->em           = $em;
        $this->router       = $router;
        $this->params       = $params;
        $this->accessKey    = $params->get('text_service_access_key');
        $this->secretKey    = $params->get('text_service_secret_key');
        $this->baseUrl      = $params->get('text_service_base_url');
        
        $this->client       = new \GuzzleHttp\Client();
        $this->headers      = [
                'User-Agent'      => $params->get('user_agent'),
                'Content-Type'    => 'application/vnd.api+json',
                'X-Spirit-Animal' => 'Rabid Lemur'
            ];
    }

    public function setTo(string $toNumber)
    {
        $this->toNumber = $toNumber;
    }

    public function setFrom(string $fromNumber)
    {
        $this->fromNumber = $fromNumber;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    public function setImages(array $images)
    {
        $this->images = $images;
    }

    public function setDlrCallback(string $url)
    {
        $this->dlrCallback = $url;
    }

    public function sendSMS()
    {
        $attributes = [
            'to'    => $this->toNumber,
            'from'  => $this->fromNumber,
            'body'  => $this->message
        ];

        $this->send($attributes);
    }

    public function sendMMS()
    {
        $attributes = [
            'to'            => $this->toNumber,
            'from'          => $this->fromNumber,
            'body'          => $this->message,
            #'is_mms'        => true,
            #'media_urls'    => $this->images
        ];

        if (is_array($this->images) && !empty($this->images)) {
            $attributes['is_mms']       = true;
            $attributes['media_urls']   = $this->images;
        }

        $this->send($attributes);
    }

    public function send(array $attributes)
    {
        if (empty($this->toNumber) || empty($this->fromNumber)) {
            throw new \Exception('To and From numbers must be set first');
        }
        
        $request = $this->client->request('POST', $this->baseUrl, 
        [
            'auth' => [
                $this->accessKey,
                $this->secretKey
            ],
            'body' => json_encode([
                'data' => [
                    'type'        => 'message',
                    'attributes'  => $attributes
                ]   
            ]),
            'headers' => $this->headers
            
        ]);
            
        $body       = $request->getBody();
        $statusCode = $request->getStatusCode();
        $json       = json_decode($body);    

        // Create a database record
        $textMessage = new FlowrouteMessage();
        $textMessage->setType($this->getType($attributes));
        $textMessage->setDirection(self::OUTBOUND);
        $textMessage->setStatusCode($statusCode);
        $textMessage->setToNumber($this->toNumber);
        $textMessage->setFromNumber($this->fromNumber);
        $textMessage->setMessage($this->message);
        
        if ($statusCode >= 200) {
            $mdr    = $json->data->id;
            $cost   = $json->data->price_details->charged_cost;
            $count  = $json->data->price_details->segment_count;

            $textMessage->setCost($cost);
            $textMessage->setMdr($mdr);
            $textMessage->setSegment($count);
        }

        $textMessage->setResponse($body);

        $this->em->persist($textMessage);
        $this->em->flush();
    }

    /**
     * @param string $callbackData
     * 
     * Expects a JSON string sent to the callback
     */
    public function receive(string $callbackData)
    {
        if (!empty($callbackData)) { 
            
            try {
                $json       = json_decode($callbackData);
                $mdr        = $json->data->id;
                $cost       = $json->data->attributes->amount_display;
                $message    = $json->data->attributes->body;
                $attributes = (array) $json->data->attributes;
                $type       = $this->getType($attributes);
                $from       = $json->data->attributes->from;
                $to         = $json->data->attributes->to;

                $textMessage = new FlowrouteMessage();
                $textMessage->setMdr($mdr);
                $textMessage->setType($type);
                $textMessage->setDirection(self::INBOUND);
                $textMessage->setStatusCode(200);
                $textMessage->setToNumber($to);
                $textMessage->setFromNumber($from);
                $textMessage->setMessage($message);
                $textMessage->setResponse($callbackData);
            
                $this->em->persist($textMessage);
            
                if ($type == self::MMS && isset($json->included) && count($json->included) >= 1) {

                    foreach ($json->included as $file) {
                        $filename   = isset($file->attributes->file_name) ? $file->attributes->file_name : null;
                        $filesize   = isset($file->attributes->file_size) ? $file->attributes->file_size : null;
                        $mimeType   = isset($file->attributes->mime_type) ? $file->attributes->mime_type : null;
                        $url        = isset($file->attributes->url) ? $file->attributes->url : null;
                        // This could be more robust a 4xx status will cause a warning with file_get_contents
                        $remoteFile = !empty($url) ? @file_get_contents($url) : null;

                        $media = new FlowrouteMessageMedia();
                        $media->setMessage($textMessage);
                        $media->setFilename($filename);
                        $media->setFilesize($filesize);
                        $media->setMimetype($mimeType);
                        $media->setRemoteUrl($url);
                        $media->setFile(base64_encode($remoteFile));

                        $this->em->persist($media);
                    }
                }
            
                $this->em->flush();
            
            } catch (\Exception $e) {

                $textMessage = new FlowrouteMessage();
                $textMessage->setType(self::SMS);
                $textMessage->setDirection(self::INBOUND);
                $textMessage->setToNumber(self::ERROR_NUMBER);
                $textMessage->setFromNumber(self::ERROR_NUMBER);
                $textMessage->setResponse($callbackData);

                $this->em->persist($textMessage);
                $this->em->flush();
            }
        }
    }

    public function dlr()
    {

    }

    public function mdr()
    {

    }

    /**
     * @param array $attributes
     * 
     * Parses the attributes to determine if message type is MMS
     */
    private function getType(array $attributes) 
    {
        $type = self::SMS;
        if (isset($attributes['is_mms']) && $attributes['is_mms'] === true) {
            $type = self::MMS;
        }

        return $type;
    }

    /**
     * Test function for verifying that your Flowroute account and settings are correct
     * 
     * @see https://manage.flowroute.com
     */
    public function test(string $toNumber, string $fromNumber, string $type = self::MMS)
    {
        if (\strlen($toNumber) < 10 || \strlen($fromNumber) < 10) {
            return [
                'data' => [
                    'message' => 'Make sure the numbers are 10 digits (no spaces or special characters).'
                ]
            ];
        }

        if (\strlen($toNumber) == 11) {
            $toNumber = '+' . $toNumber;
        }
        if (\strlen($fromNumber) == 11) {
            $fromNumber = '+' . $fromNumber;
        }

        $this->setTo($toNumber);
        $this->setFrom($fromNumber);

        if ($type == 'MMS') {
            $message = 'This was a successful MMS message! You should have an image sent as well.';
            $this->setImages(['https://dynaimage.cdn.cnn.com/cnn/c_fill,g_auto,w_1200,h_675,ar_16:9/https%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F160107100400-monkey-selfie.jpg']);
        } else {
            $message = 'This was a sucessful SMS message!';
        }

        $this->setMessage($message);
        $this->sendMMS();

    
        return [
            'data' => [
                'message' => 'Check your phone to make sure you received the message!'
            ]
        ];
    }
}
