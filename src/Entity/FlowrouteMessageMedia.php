<?php

namespace App\Entity;

use App\Repository\FlowrouteMessageMediaRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=FlowrouteMessageMediaRepository::class)
 */
class FlowrouteMessageMedia
{
    /**
    * @var \Ramsey\Uuid\UuidInterface
    *
    * @ORM\Id()
    * @ORM\Column(type="uuid", unique=true)
    * @ORM\GeneratedValue(strategy="CUSTOM")
    * @ORM\CustomIdGenerator(class=UuidGenerator::class)
    */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FlowrouteMessage::class, inversedBy="filename")
     * @ORM\JoinColumn(nullable=false)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="integer")
     */
    private $filesize;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $mimetype;

    /**
     * @ORM\Column(type="text")
     */
    private $remote_url;

    /**
     * @ORM\Column(type="text")
     */
    private $file;

    public function getId()
    {
        return $this->id;
    }

    public function getMessage(): ?FlowrouteMessage
    {
        return $this->message;
    }

    public function setMessage(?FlowrouteMessage $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFilesize(): ?int
    {
        return $this->filesize;
    }

    public function setFilesize(int $filesize): self
    {
        $this->filesize = $filesize;

        return $this;
    }

    public function getMimetype(): ?string
    {
        return $this->mimetype;
    }

    public function setMimetype(string $mimetype): self
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    public function getRemoteUrl(): ?string
    {
        return $this->remote_url;
    }

    public function setRemoteUrl(string $remote_url): self
    {
        $this->remote_url = $remote_url;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }
}
