<?php

namespace App\Entity;

use App\Repository\FlowrouteMessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=FlowrouteMessageRepository::class)
 */
class FlowrouteMessage
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
    *
    * @ORM\Id()
    * @ORM\Column(type="uuid", unique=true)
    * @ORM\GeneratedValue(strategy="CUSTOM")
    * @ORM\CustomIdGenerator(class=UuidGenerator::class)
    */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $toNumber;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $fromNumber;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $direction;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cost;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $segment;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mdr;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $response;

    /**
     * @ORM\OneToMany(targetEntity=FlowrouteMessageMedia::class, mappedBy="message", orphanRemoval=true)
     */
    private $filename;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->filename = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    public function setStatusCode(?int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getMdr(): ?string
    {
        return $this->mdr;
    }

    public function setMdr(?string $mdr): self
    {
        $this->mdr = $mdr;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(?float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getToNumber(): ?string
    {
        return $this->toNumber;
    }

    public function setToNumber(string $toNumber): self
    {
        $this->toNumber = $toNumber;

        return $this;
    }

    public function getFromNumber(): ?string
    {
        return $this->fromNumber;
    }

    public function setFromNumber(string $fromNumber): self
    {
        $this->fromNumber = $fromNumber;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getSegment(): ?int
    {
        return $this->segment;
    }

    public function setSegment(?int $segment): self
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * @return Collection|FlowrouteMessageMedia[]
     */
    public function getFilename(): Collection
    {
        return $this->filename;
    }

    public function addFilename(FlowrouteMessageMedia $filename): self
    {
        if (!$this->filename->contains($filename)) {
            $this->filename[] = $filename;
            $filename->setMessage($this);
        }

        return $this;
    }

    public function removeFilename(FlowrouteMessageMedia $filename): self
    {
        if ($this->filename->contains($filename)) {
            $this->filename->removeElement($filename);
            // set the owning side to null (unless already changed)
            if ($filename->getMessage() === $this) {
                $filename->setMessage(null);
            }
        }

        return $this;
    }
}
