<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\FlowrouteMessage\FlowrouteMessageService;
use App\Service\MailerService;

/** 
 * @Route("/admin/test", name="admin_test_") 
 */
class TestServicesController extends AdminBaseController
{
    private $params;
    private $mailerService;
    private $messageService;

    public function __construct(ParameterBagInterface $params, MailerService $mailerService, FlowrouteMessageService $messageService)
    {
        $this->params           = $params;
        $this->mailerService    = $mailerService;
        $this->messageService   = $messageService;
    }

    /**
     * @Route("/index", name="index")
     * 
     * Checks for required and optional .env keys 
     */
    public function indexCheck(Request $request)
    {
        $settings =$this->checkSettings();

        return $this->render('Admin/check/index.html.twig', [
            'settings' => $settings,
        ]);
    }

    public function messageCheck(Request $request) 
    {
        $response = null;

        if ($request->isMethod('POST')) {
            $response = $this->messageTest();
        }

        return $this->render('Admin/check/message.html.twig', [

        ]);
    }

    public function mailCheck(Request $request)
    {
        return $this->render('Admin/check/mail.html.twig', [
            
        ]);
    }

    private function checkSettings()
    {
        $devMode = $this->params->get('app_env');

        if ($devMode != 'dev') {
            // Only allow this page for admins in dev mode.
            throw new \Exception('Nope');
        }

        $options    = [];
        $errors     = [];
        $warnings   = [];
        $envKeys    = [
            'site_name', 
            'site_tagline', 
            'site_url',
            'physical_mailing_address', 
            'allow_self_register',
            'password_min_length',
            'password_token_ttl',
            'require_password_uppercase',
            'require_password_lowercase',
            'require_password_number',
            'require_password_specialchars',
            'user_agent',
        ];
        $optionalKeys = [
            'sendgrid_api_key',
            'mailer_from_address',
            
            'text_service_access_key',
            'text_service_secret_key',
            'text_service_base_url',
        ];

        foreach ($envKeys as $k) {
            if (!$this->params->has($k)) {
                $errors[] = 'Missing env setting for: '.$k;
            } elseif (empty($this->params->get($k))) {
                $warnings[] = 'Missing value for env setting: '.$k;
            }
        }

        foreach ($optionalKeys as $o) {
            if (!$this->params->has($o) || empty($this->params->get($o))) {
                $options[] = 'You have no env setting for: '.$o;
            }
        }
        
        return [
            'data' => [
                'require_environment_keys' => [
                    'errors'    => $errors,
                    'warnings'  => $warnings
                ],
                'optional_environment_keys' => [
                    'message' => "If you're missing any of these you cannot use these features",
                    'warnings' => $options
                ]
            ]
        ];
    }

    /**
     * @Route("/message", name="message", methods={"POST"})
     * 
     * @see FlowrouteMessageService::test
     */
    public function messageTest(Request $request)
    {
        $toNumber   = $request->request->get('toNumber');
        $fromNumber = $request->request->get('fromNumber');
        $type       = $request->request->get('type');

        $response = $this->messageService->test($toNumber, $fromNumber, $type);
                
        $this->addFlash('success', $response['data']['message']);

        return $this->redirectToRoute('admin_test_index');
    }

    /**
     * @Route("/mail", name="mail", methods={"POST"})
     * 
     * @see MailerService::test
     * 
     * Sends a test email to the email address specified
     */
    public function mailTest(Request $request)
    {
        $email = $request->request->get('toAddress');
        $response = $this->mailerService->test($email);

        return $this->json($response);   
    }

}