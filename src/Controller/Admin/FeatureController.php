<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\AccountFeature;
use App\Entity\Account;

/** 
 * @Route("/admin/feature", name="admin_feature_") 
 */
class FeatureController extends AdminBaseController
{
    /**
     * @Route("/list", name="list")
     */
    public function featureList()
    {
        $repo       = $this->getDoctrine()->getRepository(AccountFeature::class);
        $features   = $repo->listFeature();

        return $this->render('Admin/feature/list.html.twig', [
            'features' => $features,
        ]);
    }

    /**
     * @Route("/{account}/toggle/{feature}", name="toggle")
     */
    public function toggleFeature(Account $account, AccountFeature $feature)
    {
        $value = true;
        if ($feature->getValue() == 1) {
            $value = false;
        }
        $feature->setValue($value);
        $this->getDoctrine()->getManager()->persist($feature);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_account_edit', ['account' => $account->getId()]);
    }


}
