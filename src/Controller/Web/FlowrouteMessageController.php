<?php

namespace App\Controller\Web;

use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FlowrouteMessage\FlowrouteMessageService;


/** 
 * @Route("/flowroute", name="flowroute_") 
 */
class FlowrouteMessageController extends BaseController
{
    /**
     * @Route("/message", name="message")
     */
    public function incoming(Request $request, FlowrouteMessageService $messageService)
    {
        $data = $request->getContent();
        $messageService->receive($data);
        
        return $this->sendJson(['data' => 'ok']);
    }

}
